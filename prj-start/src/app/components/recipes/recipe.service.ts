import {Recipe} from './recipe.model';
import {Injectable} from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shoppinglist.service';
import {Subject} from 'rxjs';

@Injectable()
export class RecipeService {
    recipeSelected = new Subject<Recipe>();
    private recipes: Recipe[] = [
        new Recipe('Test recipe',
            'This is a tasty pasta recipe',
            'https://www.inspiredtaste.net/wp-content/uploads/2019/02/Vegetable-Spaghetti-Recipe-2-1200.jpg',
            [
                new Ingredient('Meat', 1),
                new Ingredient('French fries', 20),
                new Ingredient('Mayones', 4)
            ]),
        new Recipe('Test recipe #2',
            'bla-bla-bla',
            'https://www.inspiredtaste.net/wp-content/uploads/2019/02/Vegetable-Spaghetti-Recipe-2-1200.jpg',
            [
                new Ingredient('Fish', 1),
                new Ingredient('Carrots', 3),
                new Ingredient('Onions', 2)
            ])
    ];

    constructor(private slService: ShoppingListService) {
    }

    getRecipes() {
        return this.recipes.slice();
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    getRecipe(id: number) {
        return this.recipes[id];
    }
}
